Tools Used in this API Rest assured framework -
  - Java
  - TestNg 
  - HTML Emailable Reports
  - Maven
  - Page Object Model
  
===============================================================
 Project Information -
  - Automating the bleze demo UI application with different parameters
  - Used the Page Object model design pattern
  - Test cases will be present in src/test/java
  - Used Json as test data - where each test class has corresponding test json file with name same as test class
  - Test data json files will be present at src/test/resources package
  
===============================================================
Steps to run this project -
  - There are two ways we can run this project
  1) Using the eclipse editor
    - Open the testng.xml created in the project
    - Right click on the file and select run as -> Testng Test
    - Reports will be created at location test-output - file name - "emailable-report.html" will have the test case reports
  2) We can run the project using maven commands using command prompt
    - Open command prompt
    - Navigate to the POM.xml location of the project
    - Run the command - mvn clean test
    - If you want to run the project with different URI, use the below command to run
      mvn clean test -Durl=https://blazedemo.com/ and if you run mvn clean test [Without -Durl argument], it will pick the url which is updated in config.prop file
  3) In eclipse editor if you face any issue like - Unable to create temp dir for webdriver, launch the eclipse using admin user and run the cases
  
