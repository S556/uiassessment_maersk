package com.maersk.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import java.util.concurrent.TimeUnit;

public class DriverFactory implements BaseConstants {

	private WebDriver driver;

	/**
	 * @return W
	 */
	protected WebDriver getDriver() {
		try {
			String browser = TestConfig.getBrowser();
			if (browser.equalsIgnoreCase(CHROME)) {
				setBrowserDriver("webdriver.chrome.driver");
				driver = new ChromeDriver();
			}else if (browser.equalsIgnoreCase(FIREFOX)) {
				setBrowserDriver("webdriver.gecko.driver");
				driver = new FirefoxDriver();
			} else if (browser.equalsIgnoreCase(IE)) {
				setBrowserDriver("webdriver.ie.driver");
				driver = new InternetExplorerDriver();
			}
			driver.manage().timeouts().implicitlyWait(Long.parseLong(TestConfig.getProperty("pageLoadTimeOut")), TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(Long.parseLong(TestConfig.getProperty("implicitWait")), TimeUnit.SECONDS);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return driver;
	}

	private void setBrowserDriver(String key) {
		if (key.contains("chrome"))
			System.setProperty(key, CHROME_DRIVER_WINDOWS);
		else if (key.contains("gecko"))
			System.setProperty(key, FIREFOX_DRIVER_WINDOWS);
		else if (key.contains("ie"))
			System.setProperty(key, IE64_DRIVER_WINDOWS);
		else if (key.contains("edge"))
			System.setProperty(key, MSEDGE_DRIVER_WINDOWS);
	}

}
