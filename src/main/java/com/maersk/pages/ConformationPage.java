package com.maersk.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import com.maersk.core.BasePageObject;

public class ConformationPage extends BasePageObject {
	private final WebDriver driver;

	@FindBy(xpath = "//div[contains(@class,'container')]/h1")
	WebElement headerTitle;

	@FindBy(xpath = "//tr[1]/td[2]")
	WebElement confId;

	@FindBy(xpath = "//tr[2]/td[2]")
	WebElement confStatus;


	public ConformationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Method to verify the conf page title
	 * @param title
	 */
	public void verifyConformationPageTitle(String title) {
		waitForElement(driver, headerTitle, 10);
		Assert.assertEquals(getText(headerTitle), title);
	}

	/**
	 * Method to verify the conf id and status
	 * @param statusMsg
	 */
	public void verifyConfIDandStatus(String statusMsg) {
		if(isDisplayed(confId)) {
			logger.info("Conf Id -"+ getText(confId));
			logger.info("Conf Status -"+ getText(confStatus));
			Assert.assertEquals(getText(confStatus), statusMsg);
		} else {
			Assert.fail("Conformation id is not displayed");
		}
	}




}
