package com.maersk.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.maersk.core.BasePageObject;
import com.maersk.core.TestConfig;

public class HomePage extends BasePageObject {
	private final WebDriver driver;

	@FindBy(xpath = "//select[@name='fromPort']")
	WebElement deptCityEle;

	@FindBy(xpath = "//select[@name='toPort']")
	WebElement destCityEle;

	@FindBy(xpath = "//*[@value='Find Flights']")
	WebElement findFlightsButton;

	@FindBy(xpath = "//tr[2]/td[1]/input[@type='submit']")
	WebElement clickChooseThisFlight;
	
	@FindBy(xpath = "//tr[2]/td/input")
	WebElement secondFlightButton;

	public HomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public boolean launchUrl() {
		navigateToUrl(driver, TestConfig.getUrl());
		return true;
	}

	/**
	 * Method to select the departure and dest city
	 * @param depCity
	 * @param destCity
	 */
	public void selectDeptDestCity(String depCity, String destCity) {
		waitForElement(driver, deptCityEle, 20);
		Select selectDept = new Select(deptCityEle);
		selectDept.selectByVisibleText(depCity);
		Select selectDest = new Select(destCityEle);
		selectDest.selectByVisibleText(destCity);
	}

	/**
	 * Method to click on find flights button
	 */
	public void clickOnFindFlights() {
		clickOnElement(findFlightsButton);
	}
	
	/**
	 * Method to click on second flight button
	 */
	public void clickOnSecondFlightBtn() {
		Assert.assertTrue(clickOnElement(secondFlightButton));
	}

}
