package com.maersk.core;

public interface BaseConstants {
    String PROJECT_PATH = System.getProperty("user.dir");
    String CHROME_DRIVER_WINDOWS = "src/main/resources/drivers/windows/chromedriver.exe";
    String FIREFOX_DRIVER_WINDOWS = "src/main/resources/drivers/windows/geckodriver.exe";
    String IE64_DRIVER_WINDOWS = "src/main/resources/drivers/windows/IEDriverServer.exe";
    String MSEDGE_DRIVER_WINDOWS = "src/main/resources/drivers/windows/msedgedriver.exe";
    

    String BROWSER = "browser";
    String OS = System.getProperty("os.name");
    String CHROME = "chrome";
    String FIREFOX = "firefox";
    String IE = "ie";
    String TRUE = "true";
    String FALSE = "false";
    String URL = "url";
    String CONFIG_PATH = "config/config.properties";

}
