package com.maersk.base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.maersk.core.TestBase;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Reporter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.*;

public class ReadTestData extends TestBase {
	public static Logger logger = Logger.getLogger(ReadTestData.class);
	String tc_Name;
	public JSONObject actualData = new JSONObject();

	public Map<String, String> getTestData(String classAndMethod[]) {
		logger.info("Class Name Executing :" + classAndMethod[0]);
		logger.info("Method Name Executing :" + classAndMethod[1]);
		String tc_id = getTc_IdByClassAndMethod(classAndMethod);
		logger.info("TC ID is:" + tc_id);
		String testDataFilePath = System.getProperty("user.dir") + "/src/test/resources/TestData/" + classAndMethod[0]
				+ ".json";
		String testdataString = readFileUsingPath(testDataFilePath);
		JSONArray testdataJson = new JSONArray(testdataString);
		Map<String, String> testDataListMap = new HashMap<String, String>();

		for (int k = 0; k < testdataJson.length(); k++) {
			JSONObject testdataObjParent = testdataJson.getJSONObject(k);
			if (testdataObjParent.getString("TC_ID").equals(tc_id)) {
				Set<String> keyName = testdataObjParent.keySet();
				for (String key : keyName) {
					testDataListMap.put("TC_ID", tc_id + "");
					testDataListMap.put(key, testdataObjParent.getString(key));
				}
				break;

			}

		}
		return testDataListMap;
	}

	public String getTc_IdByClassAndMethod(String arr[]) {
		String testDataFilePath = System.getProperty("user.dir") + "/src/test/resources/TestData/" + arr[0] + ".json";
		String tc_id = "";
		String testdataString = readFileUsingPath(testDataFilePath);
		JSONArray testdataJsonArray = new JSONArray(testdataString);
		for (int i = 0; i < testdataJsonArray.length(); i++) {
			JSONObject testMethodJson = testdataJsonArray.getJSONObject(i);
			if (testMethodJson.get("TC_ID").equals(arr[1])) {
				tc_id = testMethodJson.get("TC_ID").toString();
			}
		}
		return tc_id;
	}

	public String readFileUsingPath(String filePath) {
		StringBuilder sb = new StringBuilder();

		String file = filePath;
		try {
			try (FileInputStream is = new FileInputStream(file)) {
				try (InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8)) {
					try (BufferedReader br = new BufferedReader(isr)) {

						String line = br.readLine();

						while (line != null) {
							sb.append(line);
							sb.append("\n");
							line = br.readLine();
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	public String[] getCurrentTest() {
		String arr[] = new String[2];
		String fullName[] = Thread.currentThread().getStackTrace()[2].getClassName().split("\\.");
		String className = fullName[fullName.length - 1];
		String method = Thread.currentThread().getStackTrace()[2].getMethodName();
		arr[0] = className;
		arr[1] = method;
		try {
			tc_Name = getTestCaseName(arr) + "";
		} catch (SQLException e) {
			Reporter.log("Unable to Fecth the Test Case Name" + arr[0] + "And " + arr[1], true);
		}
		return arr;
	}

	public String getTestCaseName(String arr[]) throws SQLException {
		JSONObject testData = readTestClassJsonFileAndReturnAllTestCases(arr[0], arr[1]);
		return testData.getString("TEST_CLASS_NAME");
	}

	public JSONObject readTestClassJsonFileAndReturnAllTestCases(String className, String methodName) {
		JSONObject testMethodDataJson = new JSONObject();
		String testDataFilePath = System.getProperty("user.dir") + "/src/test/resources/TestData/" + className
				+ ".json";
		String testdataString = readFileUsingPath(testDataFilePath);
		JSONArray testClassDataJsonArray = new JSONArray(testdataString);
		for (int i = 0; i < testClassDataJsonArray.length(); i++) {
			JSONObject testData = testClassDataJsonArray.getJSONObject(i);
			if (testData.get("TC_ID").equals(methodName)) {
				testMethodDataJson = testData;
			}
		}
		return testMethodDataJson;
	}

	protected String getJson(Object obj) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(obj);
		JSONObject jsonObj = new JSONObject(json);
		return jsonObj.toString();
	}


}
