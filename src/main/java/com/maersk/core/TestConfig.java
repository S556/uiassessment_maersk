package com.maersk.core;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class TestConfig implements BaseConstants {

	private static final String browser = System.getProperty(BROWSER);
	private static String url = System.getProperty(URL);
	private static Properties properties;

	public static String getProperty(String prop) {
		if (properties.containsKey(prop)) {
			return properties.getProperty(prop);
		}
		return null;
	}

	public static String getBrowser() {
		if (browser == null || browser.isEmpty()) {
			return getProperty(BROWSER);
		} else {
			return browser;
		}
	}

	public static void loadProperties() {
		try {
			URL configPath = TestConfig.class.getClassLoader().getResource(CONFIG_PATH);
			FileInputStream fileInputStream = new FileInputStream(configPath.getFile());
			properties = new Properties();
			properties.load(fileInputStream);
		} catch (IOException ex) {
			ex.getStackTrace();
		}
	}


	public static String getUrl() {
		return setUrl();
	}

	private static String setUrl() {
		if (url == null || url.isEmpty()) {
			url = getProperty(URL);
			if (url.endsWith("/"))
				return url;
			else
				return url + "/";
		} else {
			if (url.endsWith("/"))
				return url;
			else
				return url + "/";
		}

	}



}
