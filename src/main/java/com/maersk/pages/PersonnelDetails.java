package com.maersk.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.maersk.core.BasePageObject;

public class PersonnelDetails extends BasePageObject {
	private final WebDriver driver;

	@FindBy(xpath = "//div[@class='container']/h2")
	WebElement headerTitle;

	@FindBy(xpath = "//div[@class='container']/p[1]")
	WebElement airlineType;

	@FindBy(xpath = "//div[@class='container']/p[2]")
	WebElement flightNo;

	@FindBy(xpath = "//div[@class='container']/p[3]")
	WebElement priceEstimated;

	@FindBy(xpath = "//div[@class='container']/p[4]")
	WebElement taxesAndFee;

	@FindBy(xpath = "//div[@class='container']/p[5]")
	WebElement totalPrice;

	@FindBy(xpath = "//label[@for='inputName']/following::input[1]")
	WebElement name;

	@FindBy(xpath = "//label[@for='address']/following::input[1]")
	WebElement address;

	@FindBy(xpath = "//label[@for='city']/following::input[1]")
	WebElement city;

	@FindBy(xpath = "//label[@for='state']/following::input[1]")
	WebElement state;

	@FindBy(xpath = "//label[@for='zipCode']/following::input[1]")
	WebElement zipcode;

	@FindBy(xpath = "//label[@for='creditCardType']/following::select[1]")
	WebElement cardType;

	@FindBy(xpath = "//label[@for='creditCardNumber']/following::input[1]")
	WebElement cardNo;

	@FindBy(xpath = "//label[@for='creditCardMonth']/following::input[1]")
	WebElement cardMonth;

	@FindBy(xpath = "//label[@for='creditCardYear']/following::input[1]")
	WebElement cardYear;

	@FindBy(xpath = "//label[@for='nameOnCard']/following::input[1]")
	WebElement nameOnCard;

	@FindBy(xpath = "//input[@value='Purchase Flight']")
	WebElement purchaseFlightButton;

	@FindBy(xpath = "//input[@type='checkbox']")
	WebElement rememberMe;

	public PersonnelDetails(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	/**
	 * Method to verify the personnel details page headers
	 * @param titleMsg
	 * @param airlineType
	 * @param fNo
	 * @param price
	 * @param priceTaxes
	 * @param totalCost
	 */
	public void verifyHeadersAndMessages(String titleMsg, String airType, String fNo, String priceWithoutTax,
			String priceTaxes, String totalCostwithTaxes) {
		waitForElement(driver, headerTitle, 30);
		Assert.assertEquals(getText(headerTitle), titleMsg);
		Assert.assertEquals(getText(airlineType).split(":")[1].trim(), airType);
		Assert.assertEquals(getText(flightNo).split(":")[1].trim(), fNo);
		Assert.assertEquals(getText(priceEstimated).split(":")[1].trim(), priceWithoutTax);
		Assert.assertEquals(getText(taxesAndFee).split(":")[1].trim(), priceTaxes);
		Assert.assertEquals(getText(totalPrice).split(":")[1].trim(), totalCostwithTaxes);
	}

	/**
	 * Method to fill the personnel address details
	 * @param addressDetails
	 */
	public void fillAddressDetails(String addressDetails) {
		String addrDetails[] = addressDetails.split(";;");
		setText(name, addrDetails[0].split("::")[1]);
		setText(address, addrDetails[1].split("::")[1]);
		setText(city, addrDetails[2].split("::")[1]);
		setText(state, addrDetails[3].split("::")[1]);
		setText(zipcode, addrDetails[4].split("::")[1]);
	}

	/**
	 * Method to fill the credit card details
	 * @param cardDetails
	 */
	public void fillCardDetails(String cardDetails) {
		String cardDet[] = cardDetails.split(";;");
		Select sel = new Select(cardType);
		sel.selectByVisibleText(cardDet[0].split("::")[1]);
		setText(cardNo, cardDet[1].split("::")[1]);
		setText(cardMonth, cardDet[2].split("::")[1]);
		setText(cardYear, cardDet[3].split("::")[1]);
		setText(nameOnCard, cardDet[4].split("::")[1]);
	}

	/**
	 * Method to click on Purchase flights button
	 */
	public void clickOnPurchaseFlights() {
		Assert.assertTrue(clickOnElement(purchaseFlightButton));
	}

	/**
	 * Method to select Remember Me text box
	 */
	public void selectRememberme() {
		Assert.assertTrue(clickOnElement(rememberMe));
	}



}
