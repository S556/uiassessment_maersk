package com.maersk.core;

import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public abstract class BasePageObject implements BaseConstants {
	public static Logger logger = Logger.getLogger(BasePageObject.class);


	/**
	 * Navigate to url
	 *
	 * @param url
	 * @param driver
	 * @return
	 */
	public boolean navigateToUrl(WebDriver driver, String url) {
		try {
			logger.info("Navigating to - " + url);
			driver.manage().window().maximize();
			driver.navigate().to(url);
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Clicks on provided element
	 *
	 * @param element
	 * @return
	 */
	public boolean clickOnElement(WebElement element) {
		try {
			element.click();
			return true;
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Is element displayed
	 *
	 * @param element
	 * @return
	 */
	public boolean isDisplayed(WebElement element) {
		try {
			return element.isDisplayed();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}


	/**
	 * Clicks on provided element
	 *
	 * @param xpath
	 * @return
	 */
	public boolean clickOnElement(WebDriver driver, String xpath) {
		try {
			WebElement element = driver.findElements(By.xpath(xpath)).get(0);

			if (element.isDisplayed()) {
				element.click();
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}


	/**
	 * Javascript click on element
	 *
	 * @param driver
	 * @param element
	 * @return
	 */
	public boolean clickOnElementJS(WebDriver driver, WebElement element) {
		try {
			if (element.isDisplayed()) {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}


	/**
	 * Set text in textbox
	 *
	 * @param element
	 * @param text
	 * @return
	 */
	public boolean setText(WebElement element, String text) {
		try {
			if (element.isDisplayed()) {
				element.click();
				element.clear();
				element.sendKeys(text);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Clear text fields
	 *
	 * @param element
	 * @return
	 */
	public boolean clearText(WebElement element) {
		try {
			if (element.isDisplayed()) {
				element.clear();
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Wait for element
	 *
	 * @param driver
	 * @param element
	 * @param timeout
	 * @return
	 */
	public WebElement waitForElement(WebDriver driver, WebElement element, long timeout) {
		try {
			return new WebDriverWait(driver, timeout).ignoring(NoSuchElementException.class)
					.pollingEvery(Duration.ofMillis(500)).until(ExpectedConditions.elementToBeClickable(element));
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}


	/**
	 * @param driver
	 * @param xpath
	 * @param timeout
	 * @return
	 */
	public WebElement waitForElement(WebDriver driver, String xpath, long timeout) {
		try {
			List<WebElement> element = driver.findElements(By.xpath(xpath));
			return new WebDriverWait(driver, timeout).ignoring(NoSuchElementException.class)
					.pollingEvery(Duration.ofMillis(500)).until(ExpectedConditions.elementToBeClickable(element.get(0)));
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Returns text of the element
	 *
	 * @param element
	 * @return
	 */
	public String getText(WebElement element) {
		try {
			return element.getText();
		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	 /**
     * Find element for xpath
     *
     * @param driver
     * @param xpath
     * @return
     */
    public WebElement findWebElement(WebDriver driver, String xpath) {
        try {
            return driver.findElement(By.xpath(xpath));
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }

    }


}
