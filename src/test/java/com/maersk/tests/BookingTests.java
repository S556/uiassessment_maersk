package com.maersk.tests;

import com.maersk.base.ReadTestData;
import com.maersk.pages.ConformationPage;
import com.maersk.pages.HomePage;
import com.maersk.pages.PersonnelDetails;

import java.util.Map;

import org.testng.annotations.Test;

public class BookingTests extends ReadTestData {
	Map<String, String> testData;
	HomePage homePage;
	PersonnelDetails personnelDetails;
	ConformationPage conformationPage;

	@Test(description = "Book the tickets from Paris to Buenos Aires")
	public void bookFlight() {
		Map<String, String> testData = getTestData(getCurrentTest());
		homePage = new HomePage(driver);
		personnelDetails = new PersonnelDetails(driver);
		conformationPage = new ConformationPage(driver);

		homePage.launchUrl();
		homePage.selectDeptDestCity(testData.get("DepartureCity"), testData.get("DestinationCity"));
		homePage.clickOnFindFlights();
		homePage.clickOnSecondFlightBtn();
		personnelDetails.verifyHeadersAndMessages(testData.get("BookingTitleMsg"),testData.get("AirlineType"),
				testData.get("FlightNo"),testData.get("Price"),testData.get("FeesAndTaxes"), testData.get("TotalCost"));
		personnelDetails.fillAddressDetails(testData.get("AddressDetails"));
		personnelDetails.fillCardDetails(testData.get("CardDetails"));
		personnelDetails.clickOnPurchaseFlights();
		conformationPage.verifyConformationPageTitle(testData.get("ConformationTitle"));
		conformationPage.verifyConfIDandStatus("PendingCapture");
	}
	
	@Test(description = "Book the tickets from Boston to Dublin with Remember Me card type")
	public void bookFlightOtherCountry() {
		Map<String, String> testData = getTestData(getCurrentTest());
		homePage = new HomePage(driver);
		personnelDetails = new PersonnelDetails(driver);
		conformationPage = new ConformationPage(driver);

		homePage.launchUrl();
		homePage.selectDeptDestCity(testData.get("DepartureCity"), testData.get("DestinationCity"));
		homePage.clickOnFindFlights();
		homePage.clickOnSecondFlightBtn();
		personnelDetails.verifyHeadersAndMessages(testData.get("BookingTitleMsg"),testData.get("AirlineType"),
				testData.get("FlightNo"),testData.get("Price"),testData.get("FeesAndTaxes"), testData.get("TotalCost"));
		personnelDetails.fillAddressDetails(testData.get("AddressDetails"));
		personnelDetails.fillCardDetails(testData.get("CardDetails"));
		if(testData.get("RememberMe").equalsIgnoreCase("Yes"))
			personnelDetails.selectRememberme();
		personnelDetails.clickOnPurchaseFlights();
		conformationPage.verifyConformationPageTitle(testData.get("ConformationTitle"));
		conformationPage.verifyConfIDandStatus("PendingCapture");
	}


}